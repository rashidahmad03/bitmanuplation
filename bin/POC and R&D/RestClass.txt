//***Account
@RestResource(urlMapping='/Accounts/*')
global with sharing class AccountManager 
{
    
    @HttpGet
    global static String getAccountById() {
        RestRequest request = RestContext.request;
        // grab the caseId from the end of the URL
        String AccountId = request.requestURI.substring(
          request.requestURI.lastIndexOf('/')+1);
        	Account result =  [SELECT Name,Phone,Type,Website,AnnualRevenue  FROM Account WHERE Id = :AccountId];
              String s = JSON.serialize(result);       
        return s;
    }
    
   @HttpPost
   global static String createAccount(String name, String phone,String Type,Decimal AnnualRevenue, String website)
        {
        Account acc = new Account( Name=name,Phone=phone,AnnualRevenue=AnnualRevenue,Website=website,Type=Type);
        insert acc;
             String s = JSON.serialize(acc);       
        return s;
        
    }   
}

//***Contact
@RestResource(urlMapping='/Contacts/*')
global with sharing class ContactManager 
{
    
    @HttpGet
    global static String getContactById() {
        RestRequest request = RestContext.request;
        // grab the caseId from the end of the URL
        String ContactId = request.requestURI.substring( request.requestURI.lastIndexOf('/')+1);
        
        	Contact result =  [SELECT LastName,FirstName,Email,Phone  FROM Contact WHERE Id = :ContactId];
               String s = JSON.serialize(result);       
        return s;
    }
    
   @HttpPost
   global static String createContact(String lastname, String firstname, String email, String phone)
        {
        Contact con = new Contact( LastName=lastname,FirstName=firstname,Email=email,Phone=phone);
        insert con;
        String s = JSON.serialize(con);
        return s;
            
            
            
    }   
}

//***Case
@RestResource(urlMapping='/Cases/*')
global with sharing class CaseManager {
    @HttpGet
    global static String getCaseById() {
        RestRequest request = RestContext.request;
        // grab the caseId from the end of the URL
        String caseId = request.requestURI.substring(
          request.requestURI.lastIndexOf('/')+1);
        Case result =  [SELECT CaseNumber,Subject,Status,Origin,Priority
                        FROM Case
                        WHERE Id = :caseId];
         String s = JSON.serialize(result);       
        return s;
        
    }
    @HttpPost
    global static String createCase(String subject, String status, String origin, String priority)
        {
        Case thisCase = new Case( Subject=subject,Status=status,Origin=origin,Priority=priority);
         
        insert thisCase;
             String s = JSON.serialize(thisCase);       
        return s;
        
    }   
    
}

//***Task
@RestResource(urlMapping='/Tasks/*')
global with sharing class TaskManager {
    @HttpGet
    global static String getTaskById() {
        RestRequest request = RestContext.request;
        // grab the caseId from the end of the URL
        String TaskId = request.requestURI.substring(
          request.requestURI.lastIndexOf('/')+1);
        Task result =  [SELECT Subject,Status,Priority FROM Task WHERE Id = :TaskId];
                        
                 String s = JSON.serialize(result);       
        return s;
     
    }
    @HttpPost
    global static String createCase(String subject, String status, String priority)
        {
        Task thisTask = new Task( Subject=subject,Status=status,Priority=priority);
         
        insert thisTask;
                     String s = JSON.serialize(thisTask);       
        return s;
    
    }   
    
}