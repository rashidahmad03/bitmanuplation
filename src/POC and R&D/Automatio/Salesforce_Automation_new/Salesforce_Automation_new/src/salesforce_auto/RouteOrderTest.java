package salesforce_auto;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

public class RouteOrderTest {

	public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "D:\\chromedriver_win32\\chromedriver.exe");
		
        Map<String, Object> prefs = new HashMap<String, Object>();

        prefs.put("profile.default_content_setting_values.notifications", 2);

        ChromeOptions options = new ChromeOptions();

        options.setExperimentalOption("prefs", prefs);
        
        ChromeDriver driver = new ChromeDriver(options);
        driver.get("https://dev2015-developer-edition.ap15.force.com");
        
        driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id63:j_id64:j_id69")).sendKeys("devanshu.a.sharma@royalcanin.support");
		driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id71:j_id72:j_id77")).sendKeys("Fujitsu@12343e7JTTd2sy76arV1qqnbfKOE");
		Select sel = new Select(driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id79:j_id80:j_id85")));
		sel.selectByValue("test");
		
		driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id89:j_id90:j_id93")).click();
		
		driver.manage().window().maximize();
		Thread.sleep(5000);
		//click on search field
				driver.findElement(By.cssSelector("input[title=\"Search Salesforce\"]")).sendKeys("EAST BAY ANIMAL HOSPITAL", Keys.ENTER);
			
				Thread.sleep(10000);
			
				//click on account
				driver.findElement(By.cssSelector("a[title=\"EAST BAY ANIMAL HOSPITAL\"]")).click();
				Thread.sleep(8000);
					
				//click on sales order	
				driver.findElement(By.cssSelector("a[title=\"Create Sales Order\"]")).click();
				Thread.sleep(10000);
				
				driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div/div[3]/ul/li[1]/div/input")).sendKeys("41145");
				driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div/div[3]/ul/li[2]/div/input")).sendKeys("", Keys.ENTER);
				Thread.sleep(8000);
				
				driver.findElement(By.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div[3]/div[3]/div[2]/table/tbody/tr[5]/td[4]/div/button")).click();
				Thread.sleep(20000);
				
		    
				driver.findElement(By.cssSelector("a[title=\"Pay & Submit Order\"]")).click();
				Thread.sleep(10000);
				
				driver.findElement(By.xpath("//button[contains(text(),'Payment & Submit Order')]")).click();

	}

}
