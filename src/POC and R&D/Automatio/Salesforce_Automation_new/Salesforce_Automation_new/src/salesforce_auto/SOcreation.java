package salesforce_auto;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class SOcreation {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");

		Map<String, Object> prefs = new HashMap<String, Object>();

		prefs.put("profile.default_content_setting_values.notifications", 2);

		ChromeOptions options = new ChromeOptions();

		options.setExperimentalOption("prefs", prefs);

		ChromeDriver driver = new ChromeDriver(options);

		driver.get("https://dev2015-developer-edition.ap15.force.com");

		driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id63:j_id64:j_id69"))
				.sendKeys("devanshu.a.sharma@royalcanin.support");
		driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id71:j_id72:j_id77"))
				.sendKeys("Fujitsu@12343e7JTTd2sy76arV1qqnbfKOE");
		Select sel = new Select(driver
				.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id79:j_id80:j_id85")));
		sel.selectByValue("test");

		driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id89:j_id90:j_id93"))
				.click();
		Thread.sleep(10000);
		driver.findElement(By.cssSelector("input[title=\"Search Salesforce\"]")).sendKeys("SO-20191105-4782012",
				Keys.ENTER);
		Thread.sleep(10000);
		driver.findElement(By.cssSelector("a[title=\"SO-20191105-4782012\"]")).click();
		Thread.sleep(10000);

		// ADD PROMOTION TO SALES ORDER------------------
		driver.findElement(By.xpath(
				"/html/body/div[5]/div[1]/section/div/div/div[1]/div[2]/div/one-record-home-flexipage2/forcegenerated-flexipage_giic_salesorderpage_gii__salesorder__c/flexipage-record-page-decorator/slot/flexipage-record-home-template-desktop2/div/div[2]/div[2]/slot/slot/flexipage-component2[1]/force-progressive-renderer/slot/slot/flexipage-aura-wrapper/div/div/div/div/div[1]/div/div/table/tr/td[1]/lightning-input/div/input"))
				.sendKeys("CSR10OFF");
		Thread.sleep(1000);
		WebElement element = driver.findElement(By.xpath(
				"/html/body/div[5]/div[1]/section/div/div/div[1]/div[2]/div/one-record-home-flexipage2/forcegenerated-flexipage_giic_salesorderpage_gii__salesorder__c/flexipage-record-page-decorator/slot/flexipage-record-home-template-desktop2/div/div[2]/div[2]/slot/slot/flexipage-component2[1]/force-progressive-renderer/slot/slot/flexipage-aura-wrapper/div/div/div/div/div[1]/div/div/table/tr/td[2]/button"));
		Actions actions = new Actions(driver);
		actions.moveToElement(element).click().build().perform();

		// PUSH ORDERS FOR STAGING ORDER
		// driver.findElement(By.cssSelector("a[title=\"Push Order\"]")).click();

		// ** PERFORMING PAY & SUBMIT ORDERS
		// Thread.sleep(20000);
		// driver.findElement(By.cssSelector("a[title=\"Pay & Submit
		// Order\"]")).click();
		// Thread.sleep(10000);
		// driver.findElement(By.xpath("//button[contains(text(),'Payment & Submit
		// Order')]")).click();
		// Thread.sleep(10000);
		// driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");
		// driver.get("https://royalcanin-us--d2csupport.lightning.force.com/lightning/r/gii__SalesOrder__c/a3b5C000000LfwvQAC/view");
		// Thread.sleep(10000);

		// MANUAL ALLOCATION BATCH RUN SCRIPT----------------

		// driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/header/div[3]/one-appnav/div/div/div/nav/one-app-launcher-header/button/div")).click();
		// Thread.sleep(10000);
		// driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div[2]/div/div[1]/h2/div/div[2]/div/div/div[2]/div[1]/input")).sendKeys("Code
		// Maintenance", Keys.ENTER);
		// Thread.sleep(5000);
		// driver.findElement(By.cssSelector("a[title=\"Code Maintenance\"]")).click();
		/// Thread.sleep(20000);
		// driver.switchTo().frame(0);
		// driver.findElement(By.id("j_id0:tabControllersys_lbl")).click();
		// Thread.sleep(10000);
		// driver.findElement(By.linkText("GII")).click();
		// Thread.sleep(10000);
		// driver.findElement(By.cssSelector("a[title=\"Execute
		// Allocation\"]")).click();
		// Thread.sleep(10000);

	}

}
