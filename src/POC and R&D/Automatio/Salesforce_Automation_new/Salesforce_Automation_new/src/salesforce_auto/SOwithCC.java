package salesforce_auto;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

public class SOwithCC {

	public static void main(String[] args) throws InterruptedException {
		   System.setProperty("webdriver.chrome.driver", "D:\\chromedriver_win32\\chromedriver.exe");
			
	        Map<String, Object> prefs = new HashMap<String, Object>();

	        prefs.put("profile.default_content_setting_values.notifications", 2);

	        ChromeOptions options = new ChromeOptions();

	        options.setExperimentalOption("prefs", prefs);
	        
	        ChromeDriver driver = new ChromeDriver(options);
	        driver.get("https://dev2015-developer-edition.ap15.force.com");
	        
	        driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id63:j_id64:j_id69")).sendKeys("devanshu.a.sharma@royalcanin.support");
			driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id71:j_id72:j_id77")).sendKeys("Fujitsu@12343e7JTTd2sy76arV1qqnbfKOE");
			Select sel = new Select(driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id79:j_id80:j_id85")));
			sel.selectByValue("test");
			
			driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id89:j_id90:j_id93")).click();
			Thread.sleep(10000);
			
			driver.manage().window().maximize();
			Thread.sleep(5000);
			
			//click on search field
			driver.findElement(By.cssSelector("input[title=\"Search Salesforce\"]")).sendKeys("EAST BAY ANIMAL HOSPITAL", Keys.ENTER);
			Thread.sleep(10000);
			
			//click on account
			driver.findElement(By.cssSelector("a[title=\"EAST BAY ANIMAL HOSPITAL\"]")).click();
			Thread.sleep(8000);
			
			// CREATION OF SALES ORDER-------------------
			driver.findElement(By.cssSelector("a[title=\"Create Sales Order\"]")).click();
			Thread.sleep(10000);
			Thread.sleep(10000);
			
			driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div/div[3]/ul/li[1]/div/input")).sendKeys("41145");
			driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div/div[3]/ul/li[2]/div/input")).sendKeys("00", Keys.ENTER);
			Thread.sleep(10000);
			driver.findElement(By.xpath("//button[contains(text(),'Save Order')]")).click();
			Thread.sleep(20000);
			driver.findElement(By.cssSelector("a[title=\"Pay & Submit Order\"]")).click();
			Thread.sleep(10000);
			
			// PAYMENT USING CREDIT CARD
			
			driver.findElement(By.xpath("//button[contains(text(),'Add Credit Card')]")).click();
			Thread.sleep(15000);
			driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div[2]/div/div[2]/div/div/div/div[2]/section/article[3]/div[2]/div[2]/div[1]/div/lightning-input/div[1]/input")).sendKeys("4111111111111111");
			Thread.sleep(10000);
			Select type = new Select(driver.findElement(By.name("CardType")));
			type.selectByValue("Visa");
			Thread.sleep(10000);
			Select exp = new Select(driver.findElement(By.name("ExpiryMonth")));
			exp.selectByValue("4");
			Thread.sleep(10000);
			Select yr = new Select(driver.findElement(By.name("ExpiryYear")));
			yr.selectByValue("2022");
		
			Thread.sleep(10000);
			driver.findElement(By.xpath("//button[contains(text(),'Add Card')]")).click();
			Thread.sleep(10000);
			driver.findElement(By.xpath("//button[contains(text(), 'Authorize Payment & Submit Order')]")).click();
		
	
	}

}
