package salesforce_auto;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

public class PushBulkOrder {

	public static void main(String[] args) throws InterruptedException {
		  System.setProperty("webdriver.chrome.driver", "D:\\chromedriver_win32\\chromedriver.exe");

	        Map<String, Object> prefs = new HashMap<String, Object>();

	        prefs.put("profile.default_content_setting_values.notifications", 2);

	        ChromeOptions options = new ChromeOptions();

	        options.setExperimentalOption("prefs", prefs);
	        
	        ChromeDriver driver = new ChromeDriver(options);

	        driver.get("https://dev2015-developer-edition.ap15.force.com");
	        
	        driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id63:j_id64:j_id69")).sendKeys("devanshu.a.sharma@royalcanin.support");
			driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id71:j_id72:j_id77")).sendKeys("Fujitsu@12343e7JTTd2sy76arV1qqnbfKOE");
			Select sel = new Select(driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id79:j_id80:j_id85")));
			sel.selectByValue("test");
			
			driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id89:j_id90:j_id93")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/header/div[3]/one-appnav/div/div/div/nav/one-app-launcher-header/button/div")).click();
			Thread.sleep(10000);
			driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div[2]/div/div[1]/h2/div/div[2]/div/div/div[2]/div[1]/input")).sendKeys("Sales Order Staging", Keys.ENTER);
			Thread.sleep(5000);
			driver.findElement(By.cssSelector("a[title=\"Sales Order Staging\"]")).click();
			Thread.sleep(5000);
			driver.findElement(By.cssSelector("a[title=\"Select List View\"]")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div/div[1]/div/div/div/div/div[1]/div/ul/li[8]/a")).click();
			Thread.sleep(1000);
			WebElement sel1 = driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div[2]/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr[1]/td[2]/span/span/label/span[1]"));
			sel1.click();
			Thread.sleep(1000);
			WebElement sel2 = driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div[2]/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr[16]/td[2]/span/span/label/span[1]"));
			sel2.click();
			Thread.sleep(1000);
			driver.findElement(By.cssSelector("a[title=\"Push Order\"]")).click();
	}

}
