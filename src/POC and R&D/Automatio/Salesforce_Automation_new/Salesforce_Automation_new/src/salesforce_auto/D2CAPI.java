package salesforce_auto;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class D2CAPI {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver_win32\\chromedriver.exe");
		
        ChromeDriver driver = new ChromeDriver();
        driver.get("https://dev2015-developer-edition.ap15.force.com");
        driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id63:j_id64:j_id69")).sendKeys("devanshu.a.sharma@royalcanin.support");
		driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id71:j_id72:j_id77")).sendKeys("Fujitsu@12343e7JTTd2sy76arV1qqnbfKOE");
		Select sel = new Select(driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id79:j_id80:j_id85")));
		sel.selectByValue("test");
		driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id89:j_id90:j_id93")).click();
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	    driver.switchTo().window(tabs.get(0)); //switches to new tab 
        driver.get("https://workbench.developerforce.com/login.php?startUrl=%2Fexecute.php");
        Select sl = new Select(driver.findElement(By.id("oauth_env")));
        sl.selectByVisibleText("Sandbox");
        WebElement chk = driver.findElement(By.id("termsAccepted"));
        chk.click();
        Thread.sleep(1000);
        driver.findElement(By.id("loginBtn")).click();
        Thread.sleep(5000);
        driver.findElement(By.id("username")).sendKeys("devanshu.a.sharma@royalcanin.support");
        driver.findElement(By.id("password")).sendKeys("Fujitsu@1234");
        
        driver.findElement(By.id("Login")).click();
        
        Actions builder = new Actions(driver);
        
        WebElement act = driver.findElement(By.linkText("utilities"));
        builder.moveToElement(act).build().perform();
       
        WebElement clck = driver.findElement(By.linkText("REST Explorer"));
        builder.moveToElement(clck).build().perform();
        clck.click();
        
        WebElement post = driver.findElement(By.xpath("//*[@id=\"mainBlock\"]/form/p[2]/label[2]/input"));
        post.click();
        
        driver.findElement(By.id("urlInput")).clear();
        Thread.sleep(1000);
        driver.findElement(By.id("urlInput")).sendKeys("/services/data/v45.0/sobjects/Account");
     
        driver.findElement(By.name("requestBody")).sendKeys("{\r\n" + 
        		"	\"attributes\": {\r\n" + 
        		"		\"type\": \"Account\",\r\n" + 
        		"		\"url\": \"/services/data/v45.0/sobjects/Account/\"\r\n" + 
        		"	},\r\n" + 
        		"	\"name\": \"Tom Pet Owner\",\r\n" + 
        		"	\"RecordType\": {\r\n" + 
        		"		\"Name\": \"Pet Owner\"\r\n" + 
        		"	},\r\n" + 
        		"	\"Business_Email__c\": \"tom.petowner@abc.com\",\r\n" + 
        		"	\"Bill_To_Customer_ID__c\": \"FS-003952012\",\r\n" + 
        		"	\"ShipToCustomerNo__c\": \"FS-0034500-52012\",\r\n" + 
        		"	\"giic_AddressType__c\": \"Business\",\r\n" + 
        		"	\"Validated__c\": \"false\",\r\n" + 
        		"	\"giic_Carrier__r\": {\r\n" + 
        		"		\"giic_UniqueCarrier__c\": \"Parcel\"\r\n" + 
        		"	},\r\n" + 
        		"	\"Payment_Method__c\": \"MC/V\",\r\n" + 
        		"	\"Account_Status__c\": \"Customer\",\r\n" + 
        		"	\"Insite_Customer_Type__c\": \"PETOWNER\",\r\n" + 
        		"	\"PriceBookCode__c\": \"MSRP\",\r\n" + 
        		"	\"giic_Payment_Terms__c\": \"ACH, 3% DISCOUNT\",\r\n" + 
        		"	\"Location_Code__c\": \"GII\"\r\n" + 
        		"}");
        driver.findElement(By.name("doExecute")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("codeViewPortToggler")).click();

	}

}
