package salesforce_auto;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;
import com.sforce.soap.partner.*;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.*;



public class InsiteOrderCreation {

	public static void main(String[] args) {
		try 
		{
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyyHH:mm:ss");
			SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");			
			String uniquecode = formatter.format(date);
			String TodayDate = formatter1.format(date);
			HashMap<String,String> mapresults = new HashMap<String,String>();
			String sosid;

			ConnectorConfig config = new ConnectorConfig();
			config.setUsername("devanshu.a.sharma@in.fujitsu.com.full");
			config.setPassword("Fujitsu@123454Ca1BVIEiqZtNs6oqvT8Hste5");
			PartnerConnection connection = null;
			connection = Connector.newConnection(config);			
			String sessionId = connection.getSessionHeader().getSessionId();			
			URL url = new URL("https://royalcanin-us--full.cs12.my.salesforce.com//services/data/v38.0/composite/tree/gii__SalesOrderStaging__c");

			HttpURLConnection con = (HttpURLConnection) url.openConnection();			
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			 
			
			con.setRequestProperty("Authorization", "OAuth " + sessionId);			
			final String POST_PARAMS = "{ \r\n" + 
					"   \"records\":[ \r\n" + 
					"      { \r\n" + 
					"         \"attributes\":{ \r\n" + 
					"            \"type\":\"gii__SalesOrderStaging__c\",\r\n" + 					 
					"             \"referenceId\":\""+uniquecode+"\"\r\n"+					
					"         },\r\n" + 
					"         \"giic_Account__c\":\"VTS-100000-902\",\r\n" +
					"         \"giic_ExecuteTrigger__c\":false,\r\n" + 
					"         \"giic_Source__c\":\"INSITE\",\r\n" + 
					"         \"giic_BillToCustID__c\":\"5567\",\r\n" + 
					"         \"giic_Comments__c\":\"comment for shipping\",\r\n" + 
					"         \"giic_CustEmail__c\":\"customer@gmail.com\",\r\n" + 
					"         \"giic_CustPhone__c\":\"567890\",\r\n" + 
					"         \"giic_Status__c\":\"Draft\",\r\n" + 
					"         \"giic_CustomerPODate__c\":\"2019-5-28\",\r\n" + 
					"         \"giic_ExtDocNo__c\":\"856562\",\r\n" + 
					"         \"giic_IntegrationOP__c\":\"Insert\",\r\n" + 
					"         \"giic_OrderNumber__c\":\"ON-INS-"+uniquecode+"\",\r\n" + 
					"         \"giic_Carrier__c\":\"Parcel\",\r\n" + 
					"         \"giic_DropShip__c\":false,\r\n" + 
					"         \"giic_AddressValidated__c\":true,\r\n" + 
					"         \"giic_ShipFeeCal__c\":true,\r\n" + 
					"         \"giic_CCAuthorized__c\":true,\r\n" + 
					"         \"giic_PromotionCalculated__c\":true,\r\n" + 
					"         \"giic_TaxCalculated__c\":true,\r\n" + 
					"         \"giic_TaxExempt__c\":false,\r\n" + 
					"         \"giic_WarehouseCode__c\":901,\r\n" + 
					"         \"giic_OrderDate__c\":\""+TodayDate+"\",\r\n" + 
					"         \"giic_OrderOrigin__c\":\"Web\",\r\n" + 
					"         \"giic_PaymentTerms__c\":\"30 Days\",\r\n" + 
					"         \"giic_PaymentMethod__c\":\"ACH\",\r\n" + 
					"         \"giic_ShippingCounty__c\":\"Johnson\",\r\n" + 
					"         \"giic_ShipToCity__c\":\"EMMETT\",\r\n" + 
					"         \"giic_ShipToCountry__c\":\"USA\",\r\n" + 
					"         \"giic_ShipToName__c\":\"Pet Supply\",\r\n" + 
					"         \"giic_ShipToPhone__c\":\"567890\",\r\n" + 
					"         \"giic_ShipToStateProvince__c\":\"ID\",\r\n" + 
					"         \"giic_ShipToStreet__c\":\"923 WILLIAMS RD\",\r\n" + 
					"         \"giic_AddressType__c\":\"RESIDENTIAL\",\r\n" + 
					"         \"giic_DiscountPercent__c\":\"10\",\r\n" + 
					"         \"giic_ShipToZipPostalCode__c\":\"83617-3852\",\r\n" + 
					"         \"giic_HPROMO1__c\":\"\",\r\n" + 
					"         \"giic_HPROMO2__c\":\"\",\r\n" + 
					"         \"giic_HPRVAL1__c\":\"\",\r\n" + 
					"         \"giic_HPRVAL2__c\":0,\r\n" + 
					"         \"giic_Promotion__c\":\"\",\r\n" + 
					"         \"giic_LoyaltyAmount__c\":0,\r\n" + 
					"         \"giic_FREESHIP__c\":\"CSRFREESHIP\",\r\n" + 
					"         \"SalesOrderLineStagings__r\":{ \r\n" + 
					"            \"records\":[ \r\n" + 
					"               { \r\n" + 
					"                  \"attributes\":{ \r\n" + 
					"                     \"type\":\"gii__SalesOrderLineStaging__c\",\r\n" + 
					"					  \"referenceId\":\""+uniquecode+"_1\"\r\n"+						
					"                  },\r\n" + 
					"                  \"giic_ProductSKU__c\":\"41145\",\r\n" + 
					"                  \"giic_LineNo__c\":\"OLN-SHI-"+TodayDate+"-01\",\r\n" + 
					"                  \"giic_Amount__c\":34.08,\r\n" + 
					"                  \"giic_CustomerPODate__c\":\""+TodayDate+"\",\r\n" + 
					"                  \"giic_EDIProductCode__c\":\"201822121\",\r\n" + 
					"                  \"giic_EDIPrice__c\":34.08,\r\n" + 
					"                  \"giic_WarehouseCode__c\":901,\r\n" + 
					"                  \"giic_RequiredDate__c\":\""+TodayDate+"\",\r\n" + 
					"                  \"giic_SellTo__c\":\"Pet Supply\",\r\n" + 
					"                  \"giic_StockUM__c\":\"CA\",\r\n" + 
					"                  \"giic_UPCCode__c\":\"UPC001\",\r\n" + 
					"                  \"giic_OrderQuantity__c\":1,\r\n" + 
					"                  \"giic_UnitPrice__c\":34.08,\r\n" + 
					"                  \"giic_DiscountPercent__c\":\"5\",\r\n" + 
					"                  \"giic_TaxAmount__c\":0\r\n" + 
					"               }\r\n" + 
					"            ]\r\n" + 
					"         },\r\n" + 
					"         \"SalesOrderAdditionalCharge_Staging__r\":{ \r\n" + 
					"            \"records\":[ \r\n" + 
					"               { \r\n" + 
					"                  \"attributes\":{ \r\n" + 
					"                     \"type\":\"gii__SalesOrderAdditionalChargeStaging__c\",\r\n" + 
					" 					  \"referenceId\":\"SOACS_"+uniquecode+"_1\"\r\n"+						
					"                  },\r\n" + 
					"                  \"giic_AdditionalChargeCode__c\":\"Shipping Fee\",\r\n" + 
					"                  \"giic_Promotion__c\":\"CSRFREESHIP\",\r\n" + 
					"                  \"giic_Quantity__c\":1,\r\n" + 
					"                  \"giic_UnitPrice__c\":6.99\r\n" + 
					"               }\r\n" + 
					"            ]\r\n" + 
					"         },\r\n" + 
					"         \"SalesOrderPaymentStagings__r\":{ \r\n" + 
					"            \"records\":[ \r\n" + 
					"               { \r\n" + 
					"                  \"attributes\":{ \r\n" + 
					"                     \"type\":\"gii__PaymentStaging__c\",\r\n" + 
					"					  \"referenceId\":\"PS_"+uniquecode+"_1\"\r\n"+						
					"                  },\r\n" + 
					"                  \"giic_Comments__c\":\"payment\",\r\n" + 
					"                  \"giic_PaidAmount__c\":34.08,\r\n" + 
					"                  \"giic_PaymentDate__c\":\""+TodayDate+"\",\r\n" + 
					"                  \"giic_PaymentMethod__c\":\"ACH\"\r\n" + 
					"               }\r\n" + 
					"            ]\r\n" + 
					"         }\r\n" + 
					"      }\r\n" + 
					"   ]\r\n" + 
					"}\r\n" + 
					"";

			con.setDoOutput(true);		
			
			OutputStream os = con.getOutputStream();
			os.write(POST_PARAMS.getBytes());
			os.flush();
			os.close();
			int responseCode = con.getResponseCode();
			System.out.println("POST Response Code :  " + responseCode);
			System.out.println("POST Response Message : " + con.getResponseMessage());
			//UtilClass.getSOSData(uniquecode);
			SObject objStaging = new SObject();
			objStaging.setType("gii__SalesOrderStaging__c");
			objStaging.setField("giic_OrderNumber__c", "ON-INS-"+uniquecode);
			//objStaging.setField("giic_ExecuteTrigger__c", true);
			UpsertResult[] results = connection.upsert("giic_OrderNumber__c", new SObject[] { objStaging });
			System.out.println("results :  " + results);
			//UtilClass.getSOSData(uniquecode);
			SObject objStaging1 = new SObject();
			objStaging1.setType("gii__SalesOrderStaging__c");
			objStaging1.setField("giic_OrderNumber__c", "ON-INS-"+uniquecode);
			objStaging1.setField("giic_ExecuteTrigger__c", true);
			UpsertResult[] results1 = connection.upsert("giic_OrderNumber__c", new SObject[] { objStaging1 });
			System.out.println("results1 :  " + results1);
			//"giic_OrderNumber__c", new SObject[] { objStaging }
			/*if (responseCode == HttpURLConnection.HTTP_CREATED) { //success
				
				BufferedReader in = new BufferedReader(new InputStreamReader(
						con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();
				while ((inputLine = in .readLine()) != null) {
					if(inputLine == "results")
					{

					}
					response.append(inputLine);
				} in .close();
				JSONObject jsonObj = new JSONObject(response.toString());
				System.out.println("jsonObj++"+jsonObj);
				JSONArray results = (JSONArray) jsonObj.get("results"); 

				for(int i=0;i<results.length();i++){
					Object obj = results.get(i);
					JSONObject ss = new JSONObject(obj.toString());					
					mapresults.put(ss.get("referenceId").toString(), ss.get("id").toString());					 
				}
				System.out.println(mapresults);		
				System.out.println("sosid++"+mapresults.get(uniquecode));	
				sosid = mapresults.get(uniquecode);
				System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");

				Map<String, Object> prefs = new HashMap<String, Object>();

				prefs.put("profile.default_content_setting_values.notifications", 2);

				ChromeOptions options = new ChromeOptions();

				options.setExperimentalOption("prefs", prefs);

				ChromeDriver driver = new ChromeDriver(options);
				driver.get("https://dev2015-developer-edition.ap15.force.com");

				driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id63:j_id64:j_id69")).sendKeys("devanshu.a.sharma@royalcanin.support");
				driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id71:j_id72:j_id77")).sendKeys("Fujitsu@12343e7JTTd2sy76arV1qqnbfKOE");
				Select sel = new Select(driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id79:j_id80:j_id85")));
				sel.selectByValue("test");
				Thread.sleep(10000);	
				driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id89:j_id90:j_id93")).click();
				driver.get("https://royalcanin-us--d2csupport.lightning.force.com/lightning/r/gii__SalesOrderStaging__c/"+sosid+"/view");
				Thread.sleep(10000);	
				// PUSH ORDERS FOR STAGING ORDER
			    driver.findElement(By.cssSelector("a[title=\"Push Order\"]")).click();
				Thread.sleep(15000);	
				UtilClass.ProcessSO(driver);
			} 
			else 
			{
				System.out.println("POST NOT WORKED");
			}*/
		} 		
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
