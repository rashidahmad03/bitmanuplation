package salesforce_auto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByPartialLinkText;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

import com.sun.xml.internal.rngom.binary.visitor.ChildElementFinder.Element;

public class Test_SO {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");

		Map<String, Object> prefs = new HashMap<String, Object>();

		prefs.put("profile.default_content_setting_values.notifications", 2);

		ChromeOptions options = new ChromeOptions();

		options.setExperimentalOption("prefs", prefs);

		ChromeDriver driver = new ChromeDriver(options);

		driver.get("https://dev2015-developer-edition.ap15.force.com");

		driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id63:j_id64:j_id69")).sendKeys("devanshu.a.sharma@royalcanin.support");
		driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id71:j_id72:j_id77")).sendKeys("Fujitsu@12343e7JTTd2sy76arV1qqnbfKOE");
		Select sel = new Select(driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id79:j_id80:j_id85")));
		sel.selectByValue("test");

		driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id89:j_id90:j_id93")).click();
		Thread.sleep(10000);
		/*driver.findElement(By.cssSelector("input[title=\"Search Salesforce\"]")).sendKeys("SO-20191202-4782423", Keys.ENTER);
		Thread.sleep(10000);
		driver.findElement(By.cssSelector("a[title=\"SO-20191202-4782423\"]")).click();
		Thread.sleep(10000);*/
		driver.get("https://royalcanin-us--d2csupport.lightning.force.com/lightning/r/gii__SalesOrder__c/a3b5C000000Lh0WQAS/view");
		Thread.sleep(10000);
		//String s = driver.findElement(ByPartialLinkText
		//expected_placeholder_text = element.attribute('placeholder') 
		//WebElement element =driver.findElement(By.xpath("//input[@placeholder='Enter Here...']"));
		WebElement element =driver.findElement(By.xpath("//*[@placeholder='Enter Here...']"));
		System.out.println(element.toString());
		String expected_placeholder_text = element.getAttribute("name");
		System.out.println(expected_placeholder_text);
		//driver.findElement(By.name("inptPromoCode")).sendKeys("CSR10FF");
		//Thread.sleep(10000);
		//driver.findElement(By.xpath("//button[@title='Apply']")).click();

		//System.out.println(driver.findElement(By.tagName("flexipage-aura-wrapper")).getText());
		//WebElement image= driver.findElement(By.cssSelector("")(""#my-id);
		//System.out.println(image.getAttribute("innerHTML"));
		//System.out.println(driver.getAttribute("innerHTML"));
		//storing the number of links in list

		//driver.findElements(By.tagName("input")[1.;
		// driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div/div/one-record-home-flexipage2/forcegenerated-flexipage_giic_salesorderpage_gii__salesorder__c/flexipage-record-page-decorator/slot/flexipage-record-home-template-desktop2/div/div[2]/div[2]/slot/slot/flexipage-component2[1]/force-progressive-renderer/slot/slot/flexipage-aura-wrapper/div/div/div/div/div[1]/div/div/table/tr/td[1]/lightning-input/div/input")).sendKeys("CSR100F");
		// Thread.sleep(10000);
		//     driver.findElement(By.xpath("//button[@title='Apply']")).click();
		List<WebElement> ss= driver.findElements(By.tagName("lightning-input"));
		System.out.println(ss.get(0).getText());
		//storing the size of the links
		//int i= ss.size();

		//	            //Printing the size of the string
		/*System.out.println(i);

		for(int j=0; j<i; j++)
		{
			//Printing the links
			//ss.get(j).ge;
			System.out.println(ss.get(j).getText());
		}*/
		//       driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div/div/one-record-home-flexipage2/forcegenerated-flexipage_giic_salesorderpage_gii__salesorder__c/flexipage-record-page-decorator/slot/flexipage-record-home-template-desktop2/div/div[2]/div[1]/slot/slot/flexipage-component2/force-progressive-renderer/slot/slot/flexipage-tabset2/div/lightning-tabset/div/lightning-tab-bar/ul/li[3]/a")).click();
		//     Thread.sleep(10000);
		//driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div[3]/div/one-record-home-flexipage2/forcegenerated-flexipage_giic_salesorderpage_gii__salesorder__c/flexipage-record-page-decorator/slot/flexipage-record-home-template-desktop2/div/div[2]/div[1]/slot/slot/flexipage-component2/force-progressive-renderer/slot/slot/flexipage-tabset2/div/lightning-tabset/div/slot/slot/slot/flexipage-tab2[3]/slot/flexipage-component2[1]/force-progressive-renderer/slot/slot/flexipage-aura-wrapper/div/div/article/div[3]/div/div/div/div/div[3]/div/div/table/tbody/tr/th/div")).click();
		//     WebElement element = driver.findElement(By.xpath("//span[@class='view-all-label']"));
		//     Actions actions = new Actions(driver);
		//     actions.moveToElement(element).click().build().perform();
		//     Thread.sleep(10000);
		//     driver.findElement(By.xpath("//button[@title='Edit Status']"));


		//driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div[4]/div/one-record-home-flexipage2/forcegenerated-flexipage_warehouse_shipping_order_staging_record_page1_gii__warehouseshippingorderstaging__c/flexipage-record-page-decorator/slot/flexipage-record-home-template-desktop2/div/div[1]/slot/slot/flexipage-component2[2]/force-progressive-renderer/slot/slot/flexipage-tabset2/div/lightning-tabset/div/slot/slot/slot/flexipage-tab2[2]/slot/flexipage-component2/force-progressive-renderer/slot/slot/records-lwc-detail-panel/div/div/div/records-record-layout-event-broker/slot/records-lwc-record-layout/records-record-layout2/force-record-layout-block/slot/force-record-layout-section[2]/div/div/div/slot/force-record-layout-row[1]/slot/force-record-layout-item[2]/div/div[2]/button")).click();
		//     Thread.sleep(10000);
		//     driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div[2]/div/one-record-home-flexipage2/forcegenerated-flexipage_warehouse_shipping_order_staging_record_page1_gii__warehouseshippingorderstaging__c/flexipage-record-page-decorator/slot/flexipage-record-home-template-desktop2/div/div[1]/slot/slot/flexipage-component2[2]/force-progressive-renderer/slot/slot/flexipage-tabset2/div/lightning-tabset/div/slot/slot/slot/flexipage-tab2[2]/slot/flexipage-component2/force-progressive-renderer/slot/slot/records-lwc-detail-panel/div/div/div/records-record-layout-event-broker/slot/records-lwc-record-layout/records-record-layout2/force-record-layout-block/slot/force-record-layout-section[2]/div/div/div/slot/force-record-layout-row[1]/slot/force-record-layout-item[2]/div/span/slot/slot/force-record-picklist/force-record-layout-picklist/span/lightning-picklist/lightning-combobox/div/lightning-base-combobox/div/div[1]/input")).click();
		//     Thread.sleep(10000);
		//     driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div[2]/div/one-record-home-flexipage2/forcegenerated-flexipage_warehouse_shipping_order_staging_record_page1_gii__warehouseshippingorderstaging__c/flexipage-record-page-decorator/slot/flexipage-record-home-template-desktop2/div/div[1]/slot/slot/flexipage-component2[2]/force-progressive-renderer/slot/slot/flexipage-tabset2/div/lightning-tabset/div/slot/slot/slot/flexipage-tab2[2]/slot/flexipage-component2/force-progressive-renderer/slot/slot/records-lwc-detail-panel/div/div/div/records-record-layout-event-broker/slot/records-lwc-record-layout/records-record-layout2/force-record-layout-block/slot/force-record-layout-section[2]/div/div/div/slot/force-record-layout-row[1]/slot/force-record-layout-item[2]/div/span/slot/slot/force-record-picklist/force-record-layout-picklist/span/lightning-picklist/lightning-combobox/div/lightning-base-combobox/div/div[2]/lightning-base-combobox-item[2]")).click();
		//     Thread.sleep(10000);
		//       driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div/div/one-record-home-flexipage2/forcegenerated-flexipage_warehouse_shipping_order_staging_record_page1_gii__warehouseshippingorderstaging__c/flexipage-record-page-decorator/slot/flexipage-record-home-template-desktop2/div/div[1]/slot/slot/flexipage-component2[2]/force-progressive-renderer/slot/slot/flexipage-tabset2/div/lightning-tabset/div/slot/slot/slot/flexipage-tab2[2]/slot/flexipage-component2/force-progressive-renderer/slot/slot/records-lwc-detail-panel/div/force-form-footer/div/lightning-button[2]")).click();
		//     Thread.sleep(10000);
		//     driver.findElement(By.cssSelector("a[title=\"Calculate Tax\"]")).click();
		//     Thread.sleep(12000);
		//	                 driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div[2]/div/one-record-home-flexipage2/forcegenerated-flexipage_warehouse_shipping_order_staging_record_page1_gii__warehouseshippingorderstaging__c/flexipage-record-page-decorator/slot/flexipage-record-home-template-desktop2/div/div[1]/slot/slot/flexipage-component2[2]/force-progressive-renderer/slot/slot/flexipage-tabset2/div/lightning-tabset/div/slot/slot/slot/flexipage-tab2[2]/slot/flexipage-component2/force-progressive-renderer/slot/slot/records-lwc-detail-panel/div/div/div/records-record-layout-event-broker/slot/records-lwc-record-layout/records-record-layout2/force-record-layout-block/slot/force-record-layout-section[2]/div/div/div/slot/force-record-layout-row[1]/slot/force-record-layout-item[2]/div/div[2]/button")).click();
		//	                  Thread.sleep(10000);
		//	                 driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div[2]/div/one-record-home-flexipage2/forcegenerated-flexipage_warehouse_shipping_order_staging_record_page1_gii__warehouseshippingorderstaging__c/flexipage-record-page-decorator/slot/flexipage-record-home-template-desktop2/div/div[1]/slot/slot/flexipage-component2[2]/force-progressive-renderer/slot/slot/flexipage-tabset2/div/lightning-tabset/div/slot/slot/slot/flexipage-tab2[2]/slot/flexipage-component2/force-progressive-renderer/slot/slot/records-lwc-detail-panel/div/div/div/records-record-layout-event-broker/slot/records-lwc-record-layout/records-record-layout2/force-record-layout-block/slot/force-record-layout-section[2]/div/div/div/slot/force-record-layout-row[1]/slot/force-record-layout-item[2]/div/span/slot/slot/force-record-picklist/force-record-layout-picklist/span/lightning-picklist/lightning-combobox/div/lightning-base-combobox/div/div[1]/input")).click();
		//	                  Thread.sleep(10000);
		//	                 driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div[2]/div/one-record-home-flexipage2/forcegenerated-flexipage_warehouse_shipping_order_staging_record_page1_gii__warehouseshippingorderstaging__c/flexipage-record-page-decorator/slot/flexipage-record-home-template-desktop2/div/div[1]/slot/slot/flexipage-component2[2]/force-progressive-renderer/slot/slot/flexipage-tabset2/div/lightning-tabset/div/slot/slot/slot/flexipage-tab2[2]/slot/flexipage-component2/force-progressive-renderer/slot/slot/records-lwc-detail-panel/div/div/div/records-record-layout-event-broker/slot/records-lwc-record-layout/records-record-layout2/force-record-layout-block/slot/force-record-layout-section[2]/div/div/div/slot/force-record-layout-row[1]/slot/force-record-layout-item[2]/div/span/slot/slot/force-record-picklist/force-record-layout-picklist/span/lightning-picklist/lightning-combobox/div/lightning-base-combobox/div/div[2]/lightning-base-combobox-item[2]")).click();
		//	                  Thread.sleep(10000);
		//	                   driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div/div/one-record-home-flexipage2/forcegenerated-flexipage_warehouse_shipping_order_staging_record_page1_gii__warehouseshippingorderstaging__c/flexipage-record-page-decorator/slot/flexipage-record-home-template-desktop2/div/div[1]/slot/slot/flexipage-component2[2]/force-progressive-renderer/slot/slot/flexipage-tabset2/div/lightning-tabset/div/slot/slot/slot/flexipage-tab2[2]/slot/flexipage-component2/force-progressive-renderer/slot/slot/records-lwc-detail-panel/div/force-form-footer/div/lightning-button[2]")).click();
		//	                  Thread.sleep(10000);
		//	                  driver.findElement(By.cssSelector("a[title=\"Authorize Amount\"]")).click();
		//	                  Thread.sleep(10000);
		//driver.navigate().refresh();
		//	                  Thread.sleep(10000);
		//	                  driver.findElement(By.cssSelector("a[title=\"Create Shipment Order\"]")).click();
		//	                  Thread.sleep(1000);
		//	                  driver.navigate().refresh();
		//	                  Thread.sleep(10000);
		//	                   driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div/div/one-record-home-flexipage2/forcegenerated-flexipage_warehouse_shipping_order_staging_record_page1_gii__warehouseshippingorderstaging__c/flexipage-record-page-decorator/slot/flexipage-record-home-template-desktop2/div/div[1]/slot/slot/flexipage-component2[2]/force-progressive-renderer/slot/slot/flexipage-tabset2/div/lightning-tabset/div/lightning-tab-bar/ul/li[1]/a")).click();
		//	                  Thread.sleep(10000);
		//	                  driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
		//	                  Thread.sleep(10000);
		//	                  
	}

}


