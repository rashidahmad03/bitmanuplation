package salesforce_auto;

import java.util.ArrayList;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class APIAutomation {

	public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
	
        ChromeDriver driver = new ChromeDriver();
        driver.get("https://dev2015-developer-edition.ap15.force.com");
        
        driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id63:j_id64:j_id69")).sendKeys("devanshu.a.sharma@royalcanin.support");
		driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id71:j_id72:j_id77")).sendKeys("Fujitsu@12343e7JTTd2sy76arV1qqnbfKOE");
		Select sel = new Select(driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id79:j_id80:j_id85")));
		sel.selectByValue("test");
		
		driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id89:j_id90:j_id93")).click();
		
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	    driver.switchTo().window(tabs.get(0)); //switches to new tab
	    
        driver.get("https://workbench.developerforce.com/login.php?startUrl=%2Fexecute.php");
        
        
        Select sl = new Select(driver.findElement(By.id("oauth_env")));
        sl.selectByVisibleText("Sandbox");
        
        WebElement chk = driver.findElement(By.id("termsAccepted"));
        chk.click();
        Thread.sleep(1000);
        driver.findElement(By.id("loginBtn")).click();
        Thread.sleep(5000);
       
        driver.findElement(By.id("username")).sendKeys("devanshu.a.sharma@royalcanin.support");
        driver.findElement(By.id("password")).sendKeys("Fujitsu@1234");
        
        driver.findElement(By.id("Login")).click();
        
        Actions builder = new Actions(driver);
        
        WebElement act = driver.findElement(By.linkText("utilities"));
        builder.moveToElement(act).build().perform();
       
        WebElement clck = driver.findElement(By.linkText("REST Explorer"));
        builder.moveToElement(clck).build().perform();
        clck.click();
        
        WebElement post = driver.findElement(By.xpath("//*[@id=\"mainBlock\"]/form/p[2]/label[2]/input"));
        post.click();
        
        driver.findElement(By.id("urlInput")).clear();
        Thread.sleep(1000);
        driver.findElement(By.id("urlInput")).sendKeys("/services/data/v38.0/composite/tree/gii__SalesOrderStaging__c");
        driver.findElement(By.name("requestBody")).sendKeys("{ \r\n" + 
        		"   \"records\":[ \r\n" + 
        		"      { \r\n" + 
        		"         \"attributes\":{ \r\n" + 
        		"            \"type\":\"gii__SalesOrderStaging__c\",\r\n" + 
        		"            \"referenceId\":\"1X11Aug0019\"\r\n" + 
        		"         },\r\n" + 
        		"         \"giic_Account__c\":\"VET-0000902-902\",\r\n" + 
        		"         \"giic_ExecuteTrigger__c\":false,\r\n" + 
        		"         \"giic_Source__c\":\"INSITE\",\r\n" + 
        		"         \"giic_BillToCustID__c\":\"5567\",\r\n" + 
        		"         \"giic_Comments__c\":\"comment for shipping\",\r\n" + 
        		"         \"giic_CustEmail__c\":\"customer@gmail.com\",\r\n" + 
        		"         \"giic_CustPhone__c\":\"567890\",\r\n" + 
        		"         \"giic_Status__c\":\"Draft\",\r\n" + 
        		"         \"giic_CustomerPODate__c\":\"2019-5-28\",\r\n" + 
        		"         \"giic_ExtDocNo__c\":\"856562\",\r\n" + 
        		"         \"giic_IntegrationOP__c\":\"Insert\",\r\n" + 
        		"         \"giic_OrderNumber__c\":\"ON-SHI-12142018-03\",\r\n" + 
        		"         \"giic_Carrier__c\":\"Route\",\r\n" + 
        		"         \"giic_DropShip__c\":false,\r\n" + 
        		"         \"giic_AddressValidated__c\":true,\r\n" + 
        		"         \"giic_ShipFeeCal__c\":true,\r\n" + 
        		"         \"giic_CCAuthorized__c\":false,\r\n" + 
        		"         \"giic_PromotionCalculated__c\":true,\r\n" + 
        		"         \"giic_TaxCalculated__c\":true,\r\n" + 
        		"         \"giic_TaxExempt__c\":false,\r\n" + 
        		"         \"giic_WarehouseCode__c\":901,\r\n" + 
        		"         \"giic_OrderDate__c\":\"2019-11-06\",\r\n" + 
        		"         \"giic_OrderOrigin__c\":\"Web\",\r\n" + 
        		"         \"giic_PaymentTerms__c\":\"30 Days\",\r\n" + 
        		"         \"giic_PaymentMethod__c\":\"ACH\",\r\n" + 
        		"         \"giic_ShippingCounty__c\":\"Johnson\",\r\n" + 
        		"         \"giic_ShipToCity__c\":\"EMMETT\",\r\n" + 
        		"         \"giic_ShipToCountry__c\":\"USA\",\r\n" + 
        		"         \"giic_ShipToName__c\":\"Pet Supply\",\r\n" + 
        		"         \"giic_ShipToPhone__c\":\"567890\",\r\n" + 
        		"         \"giic_ShipToStateProvince__c\":\"ID\",\r\n" + 
        		"         \"giic_ShipToStreet__c\":\"923 WILLIAMS RD\",\r\n" + 
        		"         \"giic_AddressType__c\":\"RESIDENTIAL\",\r\n" + 
        		"         \"giic_DiscountPercent__c\":\"10\",\r\n" + 
        		"         \"giic_ShipToZipPostalCode__c\":\"83617-3852\",\r\n" + 
        		"         \"giic_HPROMO1__c\":\"\",\r\n" + 
        		"         \"giic_HPROMO2__c\":\"\",\r\n" + 
        		"         \"giic_HPRVAL1__c\":\"\",\r\n" + 
        		"         \"giic_HPRVAL2__c\":0,\r\n" + 
        		"         \"giic_Promotion__c\":\"\",\r\n" + 
        		"         \"giic_LoyaltyAmount__c\":0,\r\n" + 
        		"         \"giic_FREESHIP__c\":\"CSRFREESHIP\",\r\n" + 
        		"         \"SalesOrderLineStagings__r\":{ \r\n" + 
        		"            \"records\":[ \r\n" + 
        		"               { \r\n" + 
        		"                  \"attributes\":{ \r\n" + 
        		"                     \"type\":\"gii__SalesOrderLineStaging__c\",\r\n" + 
        		"                     \"referenceId\":\"1X11Aug0019_1\"\r\n" + 
        		"                  },\r\n" + 
        		"                  \"giic_ProductSKU__c\":\"41145\",\r\n" + 
        		"                  \"giic_LineNo__c\":\"OLN-SHI-12142018-03-01\",\r\n" + 
        		"                  \"giic_Amount__c\":34.08,\r\n" + 
        		"                  \"giic_CustomerPODate__c\":\"2019-11-06\",\r\n" + 
        		"                  \"giic_EDIProductCode__c\":\"201822121\",\r\n" + 
        		"                  \"giic_EDIPrice__c\":34.08,\r\n" + 
        		"                  \"giic_WarehouseCode__c\":901,\r\n" + 
        		"                  \"giic_RequiredDate__c\":\"2019-11-08\",\r\n" + 
        		"                  \"giic_SellTo__c\":\"Pet Supply\",\r\n" + 
        		"                  \"giic_StockUM__c\":\"CA\",\r\n" + 
        		"                  \"giic_UPCCode__c\":\"UPC001\",\r\n" + 
        		"                  \"giic_OrderQuantity__c\":1,\r\n" + 
        		"                  \"giic_UnitPrice__c\":34.08,\r\n" + 
        		"                  \"giic_DiscountPercent__c\":\"5\",\r\n" + 
        		"                  \"giic_TaxAmount__c\":0\r\n" + 
        		"               }\r\n" + 
        		"            ]\r\n" + 
        		"         },\r\n" + 
        		"         \"SalesOrderAdditionalCharge_Staging__r\":{ \r\n" + 
        		"            \"records\":[ \r\n" + 
        		"               { \r\n" + 
        		"                  \"attributes\":{ \r\n" + 
        		"                     \"type\":\"gii__SalesOrderAdditionalChargeStaging__c\",\r\n" + 
        		"                     \"referenceId\":\"SOACS_1X11Aug0019_1\"\r\n" + 
        		"                  },\r\n" + 
        		"                  \"giic_AdditionalChargeCode__c\":\"Shipping Fee\",\r\n" + 
        		"                  \"giic_Promotion__c\":\"CSRFREESHIP\",\r\n" + 
        		"                  \"giic_Quantity__c\":1,\r\n" + 
        		"                  \"giic_UnitPrice__c\":6.99\r\n" + 
        		"               }\r\n" + 
        		"            ]\r\n" + 
        		"         },\r\n" + 
        		"         \"SalesOrderPaymentStagings__r\":{ \r\n" + 
        		"            \"records\":[ \r\n" + 
        		"               { \r\n" + 
        		"                  \"attributes\":{ \r\n" + 
        		"                     \"type\":\"gii__PaymentStaging__c\",\r\n" + 
        		"                     \"referenceId\":\"PS_1X11Aug0019_1\"\r\n" + 
        		"                  },\r\n" + 
        		"                  \"giic_Comments__c\":\"payment\",\r\n" + 
        		"                  \"giic_PaidAmount__c\":34.08,\r\n" + 
        		"                  \"giic_PaymentDate__c\":\"2019-5-28\",\r\n" + 
        		"                  \"giic_PaymentMethod__c\":\"ACH\"\r\n" + 
        		"               }\r\n" + 
        		"            ]\r\n" + 
        		"         }\r\n" + 
        		"      }\r\n" + 
        		"   ]\r\n" + 
        		"}\r\n" + 
        		" ");
        driver.findElement(By.name("doExecute")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("codeViewPortToggler")).click();
        WebElement ele = driver.findElement(By.xpath("//*[contains(.,'id:')]"));
        String test = ele.getText();
        
        
        //Thread.sleep(10000);
    	//ArrayList<String> tabs1 = new ArrayList<String> (driver.getWindowHandles());
	    //driver.switchTo().window(tabs1.get(1)); //switches to new tab
       // driver.get("https://dev2015-developer-edition.ap15.force.com");
        
       // driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id63:j_id64:j_id69")).sendKeys("devanshu.a.sharma@royalcanin.support");
	//	driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id71:j_id72:j_id77")).sendKeys("Fujitsu@12343e7JTTd2sy76arV1qqnbfKOE");
	//	Select sel1 = new Select(driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id79:j_id80:j_id85")));
	//	sel1.selectByValue("test");
		
	//	driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id89:j_id90:j_id93")).click();
		
	//	driver.findElement(By.cssSelector("a[title=\"Sales Order Staging\"]")).click();
	}

}
