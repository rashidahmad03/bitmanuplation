package salesforce_auto;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

public class returnordercreation {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver_win32\\chromedriver.exe");

        Map<String, Object> prefs = new HashMap<String, Object>();

        prefs.put("profile.default_content_setting_values.notifications", 2);

        ChromeOptions options = new ChromeOptions();

        options.setExperimentalOption("prefs", prefs);
        
        ChromeDriver driver = new ChromeDriver(options);

        driver.get("https://dev2015-developer-edition.ap15.force.com");
        
        driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id63:j_id64:j_id69")).sendKeys("devanshu.a.sharma@royalcanin.support");
		driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id71:j_id72:j_id77")).sendKeys("Fujitsu@12343e7JTTd2sy76arV1qqnbfKOE");
		Select sel = new Select(driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id79:j_id80:j_id85")));
		sel.selectByValue("test");
		
		driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id89:j_id90:j_id93")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/header/div[3]/one-appnav/div/div/div/nav/one-app-launcher-header/button/div")).click();
		Thread.sleep(10000);
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div[2]/div/div[1]/h2/div/div[2]/div/div/div[2]/div[1]/input")).sendKeys("Cases", Keys.ENTER);
		Thread.sleep(5000);
		driver.findElement(By.cssSelector("a[title=\"Cases\"]")).click();
	
		
		Thread.sleep(10000);
		driver.findElement(By.cssSelector("a[title=\"New\"]")).click();
		Thread.sleep(10000);
		WebElement sel1 = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div[2]/div/div[2]/div/div/div[1]/div/div/div[1]/fieldset/div[2]/div[21]/label/div[1]/span"));
		sel1.click();
		Thread.sleep(10000);
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div[2]/div/div[2]/div/div/div[2]/div/button[2]/span")).click();
		Thread.sleep(10000);
		driver.findElement(By.cssSelector("input[title=\"Search Accounts\"]")).sendKeys("John Nick", Keys.ENTER);
		Thread.sleep(10000);
		driver.findElement(By.cssSelector("a[title=\"John Nick\"]")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div[2]/div/div[2]/div/div/div[1]/div/article/div[3]/div/div[2]/div/div/div[2]/div[1]/div/div/div/input")).sendKeys("Testing Automation");
		Thread.sleep(10000);
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div[2]/div/div[2]/div/div/div[1]/div/article/div[3]/div/div[2]/div/div/div[3]/div[1]/div/div/div/textarea")).sendKeys("Testing Automation");
		Thread.sleep(10000);
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div[2]/div/div[2]/div/div/div[2]/div/div/button[3]/span")).click();
		Thread.sleep(10000);
		driver.findElement(By.cssSelector("a[title=\"Add Return Product\"]")).click();
		Thread.sleep(10000);
		driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div[4]/div/div[1]/div/div[1]/lightning-input/div/input")).sendKeys("INV-20191101-4597184");
		Thread.sleep(10000);
		driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div[4]/div/div[1]/div/div[2]/button")).click();
	    Thread.sleep(10000);
	    driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div[4]/div/div[2]/div/table[1]/tbody/tr/td[10]/div/input")).clear();
	    driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div[4]/div/div[2]/div/table[1]/tbody/tr/td[10]/div/input")).sendKeys("1");
	    Thread.sleep(10000);
	    Select sel11 = new Select(driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div[4]/div/div[2]/div/table[1]/tbody/tr/td[11]/div/select")));
	    sel11.selectByValue("CM-SHORT");
		Thread.sleep(1000);
		driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div[4]/div/div[1]/div/div[3]/div/button[1]")).click();
	}

}
