package salesforce_auto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

public class OMSOrderCreation {

	public static void main(String[] args) throws InterruptedException {
		String accid;
		String soName;
		String soId;
		String soStatus;
		String fullfillmentname;
		String fullfillmentId;
		String FulfillmentIntegrationStatus;
		HashMap<String, String> mapfulfillmentresult = new HashMap<String, String>();
		HashMap<String, String> mapshipmentresult = new HashMap<String, String>();
		HashMap<String, String> maprsoresult = new HashMap<String, String>();
		String shipmentname;

		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");

		Map<String, Object> prefs = new HashMap<String, Object>();

		prefs.put("profile.default_content_setting_values.notifications", 2);

		ChromeOptions options = new ChromeOptions();

		options.setExperimentalOption("prefs", prefs);

		ChromeDriver driver = new ChromeDriver(options);
		driver.get("https://dev2015-developer-edition.ap15.force.com");
		driver.manage().window().maximize();
		driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id63:j_id64:j_id69"))
				.sendKeys("devanshu.a.sharma@royalcanin.support");
		driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id71:j_id72:j_id77"))
				.sendKeys("Fujitsu@12343e7JTTd2sy76arV1qqnbfKOE");
		Select sel = new Select(driver
				.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id79:j_id80:j_id85")));
		sel.selectByValue("test");

		driver.findElement(By.name("j_id0:j_id9:j_id10:j_id11:j_id26:j_id27:j_id59:j_id60:j_id89:j_id90:j_id93"))
				.click();
		// Thread.sleep(10000);

		// driver.manage().window().maximize();
		// Thread.sleep(5000);

		// accid = UtilClass.getAccountId("EAST BAY ANIMAL HOSPITAL");
		accid = "0015C00000QVeqcQAD";
		// driver.get("https://royalcanin-us--d2csupport.lightning.force.com/lightning/r/Account/0015C00000QVeqcQAD/view");
		// driver.get("https://royalcanin-us--d2csupport.lightning.force.com/lightning/r/Account/"+accid+"/view");
		driver.get(
				"https://royalcanin-us--d2csupport.lightning.force.com/lightning/cmp/c__giic_SOProductEntry?c__accId="
						+ accid);
		Thread.sleep(10000);

		// CREATION OF SALES ORDER-------------------
		// driver.findElement(By.cssSelector("a[title=\"Create Sales
		// Order\"]")).click();
		// Thread.sleep(10000);

		driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div/div[3]/ul/li[1]/div/input"))
				.sendKeys("41125");
		driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div/div[3]/ul/li[2]/div/input"))
				.sendKeys("", Keys.ENTER);
		Thread.sleep(2000);
		// driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div/div[3]/ul/li[1]/div/input")).sendKeys("41145");
		// driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div/div[3]/ul/li[2]/div/input")).sendKeys("",
		// Keys.ENTER);

		driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
		Thread.sleep(10000);
		driver.findElement(By.xpath("//button[contains(text(),'Save Order')]")).click();
		Thread.sleep(15000);
		WebElement ele = driver.findElement(By.xpath(
				"/html/body/div[5]/div[1]/section/div/div/div[1]/div/div/one-record-home-flexipage2/forcegenerated-flexipage_giic_salesorderpage_gii__salesorder__c/flexipage-record-page-decorator/slot/flexipage-record-home-template-desktop2/div/div[1]/slot/slot/flexipage-component2[1]/force-progressive-renderer/slot/slot/records-lwc-highlights-panel/records-lwc-record-layout/records-record-layout2/force-highlights2/div[1]/div[1]/div[1]/div[2]/h1/slot/slot/lightning-formatted-text"));
		soName = ele.getText();
		System.out.println("**********" + soName);
		Thread.sleep(10000);

		// ADD PROMOTION TO SALES ORDER------------------

		List<WebElement> ss = driver.findElements(By.xpath("//div"));
		// storing the size of the links
		// int i= ss.size();
		String source = driver.findElement(By.xpath("/html/body")).getAttribute("innerHTML");
		System.out.println(source);

		// driver.findElement(By.cssSelector("input[type^=\"text\"][name=\"inptPromoCode\"]")).sendKeys("Asdfas");
		// Printing the size of the string
		// System.out.println(i);
		List<WebElement> element = driver.findElements(By.tagName("div"));

		element.get(2);
		// for (int i = 0; i < ss.size(); i++) {
		// if (ss.get(i).getText().equalsIgnoreCase("Promotion")) {

		// System.out.println(":>>" + ss.get(i).getText());
		// List<WebElement> sssss = ss.get(i).findElements(By.xpath("//input"));
		// for (WebElement v : sssss) {
		// v.getClass();
		// System.out.println("***" + v.getText());
		// System.out.println(v.getClass());
		// }
		// ss.get(i).findElement(By.tagName("inptPromoCode")).sendKeys("CSR10OFF");
		// System.out.println(":" + ss.get(i).findElement(By.tagName("inptPromoCode")));

		// }
		// }
		// driver.findElement(By.name("inptPromoCode")).sendKeys("CSR10OFF");
		// Thread.sleep(10000);
		// WebElement element =
		// driver.findElement(By.cssSelector("button[title=\"Apply\"]"));
		// Actions actions = new Actions(driver);
		// actions.moveToElement(element).click().build().perform();

	}
}
