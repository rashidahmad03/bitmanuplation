package salesforce_auto;
import java.util.HashMap;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


import com.sforce.soap.partner.*;
import com.sforce.soap.partner.sobject.*;
import com.sforce.ws.*;

public class UtilClass {
	
	public static String username = "devanshu.a.sharma@in.fujitsu.com.full";
	public static String pass ="Fujitsu@123454Ca1BVIEiqZtNs6oqvT8Hste5";
	
	/*public static void main(String[] args) {

		ConnectorConfig config = new ConnectorConfig();
		config.setUsername(username);
		config.setPassword(pass);

		PartnerConnection connection = null;

		try {
			// create a connection object with the credentials
			connection = Connector.newConnection(config);

			// create a new account
			System.out.println("Creating a new Account...");
			  SObject account = new SObject();
			  account.setType("Account");
			  account.setField("Name", "ACME Account 1");*/
			//SaveResult[] results = connection.create(new SObject[] { account });
			//System.out.println("Created Account: " + results[0].getId());

			// query for the 5 newest accounts
			// System.out.println("Querying for the 5 newest Accounts...");
			/*QueryResult queryResults = connection.query("SELECT Id, Name from Account " +
			      "ORDER BY CreatedDate DESC LIMIT 5");
			  if (queryResults.getSize() > 0) {
			    for (SObject s: queryResults.getRecords()) {
			     // System.out.println("Id: " + s.getField("Id") + " - Name: "+s.getField("Name"));
			    }
			  }

		} catch (ConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}*/

	public static HashMap<String,String>  getSOData(String str)
	{
		HashMap<String,String> mapresult = new HashMap<String,String>();
		ConnectorConfig config = new ConnectorConfig();
		config.setUsername(username);
		config.setPassword(pass);
		String  Id = "";
		String  giic_SOStatus__c = "";
		PartnerConnection connection = null;

		try {

			// create a connection object with the credentials
			connection = Connector.newConnection(config);

			QueryResult queryResults = connection.query("SELECT Id, Name,giic_SOStatus__c from gii__SalesOrder__c where name ="+"\'"+str+"\'"+" limit 1");

			//"ORDER BY CreatedDate DESC LIMIT 5");
			if (queryResults.getSize() > 0) {
				for (SObject s: queryResults.getRecords()) {	          
					Id = (s.getField("Id")).toString() ; 
					giic_SOStatus__c = (s.getField("giic_SOStatus__c")).toString() ;
				}
				mapresult.put("Id", Id);
				mapresult.put("giic_SOStatus__c", giic_SOStatus__c);
			}

		} catch (ConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mapresult;
	}

	public static String  getAccountId(String str)
	{
		ConnectorConfig config = new ConnectorConfig();
		config.setUsername(username);
		config.setPassword(pass);
		String  Id = "";
		PartnerConnection connection = null;

		try {

			// create a connection object with the credentials
			connection = Connector.newConnection(config);	   


			QueryResult queryResults = connection.query("SELECT Id, Name from Account where name ="+"\'"+str+"\'"+" limit 1");

			if (queryResults.getSize() > 0) {
				for (SObject s: queryResults.getRecords()) {	          
					Id = (s.getField("Id")).toString() ;
				}
			}

		} catch (ConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Id;
	}
	
	public static HashMap<String,String>  getSOSData(String TodayDate)
	{
		HashMap<String,String> mapresult = new HashMap<String,String>();
		ConnectorConfig config = new ConnectorConfig();
		config.setUsername(username);
		config.setPassword(pass);
		String  Id = "";
		PartnerConnection connection = null;

		try {

			// create a connection object with the credentials
			connection = Connector.newConnection(config);	   

			String str = "ON-INS-"+TodayDate;
			QueryResult queryResults = connection.query("SELECT Id, Name from gii__SalesOrderStaging__c where giic_OrderNumber__c ="+"\'"+str+"\'"+" limit 1");

			if (queryResults.getSize() > 0) {
				for (SObject s: queryResults.getRecords()) {	          
					Id = (s.getField("Id")).toString() ;
				}
			}
			SObject objStaging = new SObject();
			objStaging.setType("gii__SalesOrderStaging__c");
			objStaging.setField("id", Id);
			objStaging.setField("giic_ExecuteTrigger__c", true);
			SaveResult[] results = connection.update(new SObject[] { objStaging });
			System.out.println("results+++"+results);
		} catch (ConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mapresult;
	}
	
	

	public static HashMap<String,String>  getFulfillmentData(String str)
	{
		HashMap<String,String> mapresult = new HashMap<String,String>();
		ConnectorConfig config = new ConnectorConfig();
		config.setUsername(username);
		config.setPassword(pass);
		String  id = "";
		String  name = "";
		PartnerConnection connection = null;

		try {

			// create a connection object with the credentials
			connection = Connector.newConnection(config);	   

			QueryResult queryResults = connection.query("SELECT Id, Name from gii__WarehouseShippingOrderStaging__c where giic_SalesOrder__c ="+"\'"+str+"\'"+" limit 1");

			if (queryResults.getSize() > 0) {
				for (SObject s: queryResults.getRecords()) {	          
					id = (s.getField("Id")).toString() ;
					name = (s.getField("Name")).toString() ;
				}
			}
			mapresult.put("id", id);
			mapresult.put("name", name);

		} catch (ConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mapresult;
	}

	public static String  getFulfillmentIntegrationStatus(String str)
	{
		ConnectorConfig config = new ConnectorConfig();
		config.setUsername(username);
		config.setPassword(pass);
		PartnerConnection connection = null;
		String  giic_IntegrationStatus__c = "";

		try {

			// create a connection object with the credentials
			connection = Connector.newConnection(config);	   

			QueryResult queryResults = connection.query("SELECT Id, Name,giic_IntegrationStatus__c from gii__WarehouseShippingOrderStaging__c where id ="+"\'"+str+"\'"+" limit 1");

			if (queryResults.getSize() > 0) {
				for (SObject s: queryResults.getRecords()) {	          
					giic_IntegrationStatus__c = (s.getField("giic_IntegrationStatus__c")).toString() ;
				}
			}

		} catch (ConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return giic_IntegrationStatus__c;
	}

	public static void  updateFulfillment(String fullfillmentId,String status)
	{

		ConnectorConfig config = new ConnectorConfig();
		config.setUsername(username);
		config.setPassword(pass);
		//String  giic_Status__c = "";
		String  giic_IntegrationStatus__c = "";
		PartnerConnection connection = null;

		try {

			// create a connection object with the credentials
			connection = Connector.newConnection(config);	   

			QueryResult queryResults = connection.query("SELECT Id, Name,giic_Status__c,giic_IntegrationStatus__c from gii__WarehouseShippingOrderStaging__c where id ="+"\'"+fullfillmentId+"\'"+" limit 1");

			if (queryResults.getSize() > 0) {
				for (SObject s: queryResults.getRecords()) {		
					//giic_Status__c = (s.getField("giic_Status__c")).toString() ;
					giic_IntegrationStatus__c = (s.getField("giic_IntegrationStatus__c")).toString() ;
				}
			}
			if(giic_IntegrationStatus__c != "" && giic_IntegrationStatus__c.contains("Calculate")){
				SObject objFulfillment = new SObject();
				objFulfillment.setType("gii__WarehouseShippingOrderStaging__c");
				objFulfillment.setField("id", fullfillmentId);
				objFulfillment.setField("giic_Status__c", status);
				SaveResult[] results = connection.update(new SObject[] { objFulfillment });
			}

		} catch (ConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static HashMap<String,String> getShipmentData(String str)
	{
		HashMap<String,String> mapresult = new HashMap<String,String>();
		ConnectorConfig config = new ConnectorConfig();
		config.setUsername("devanshu.a.sharma@royalcanin.support");
		config.setPassword("Fujitsu@12343e7JTTd2sy76arV1qqnbfKOE");
		String  id = "";
		String  name = "";
		PartnerConnection connection = null;

		try {

			// create a connection object with the credentials
			connection = Connector.newConnection(config);	   

			QueryResult queryResults = connection.query("SELECT Id, Name from gii__WarehouseShipmentAdviceStaging__c where giic_WHSShipNo__c ="+"\'"+str+"\'"+" limit 1");

			if (queryResults.getSize() > 0) {
				for (SObject s: queryResults.getRecords()) {	          
					id = (s.getField("Id")).toString() ;
					name = (s.getField("Name")).toString() ;
				}
			}
			mapresult.put("id", id);
			mapresult.put("name", name);

		} catch (ConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mapresult;
	}

	public static void ProcessSO(ChromeDriver driver)
	{
		String accid ;
		String soName;
		String soId ;
		String soStatus ;
		String fullfillmentname ;
		String fullfillmentId;
		String FulfillmentIntegrationStatus;
		HashMap<String,String> mapfulfillmentresult = new HashMap<String,String>();
		HashMap<String,String> mapshipmentresult = new HashMap<String,String>();
		HashMap<String,String> maprsoresult = new HashMap<String,String>();
		String shipmentname ;

		
		try {

			WebElement ele = driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div/div/one-record-home-flexipage2/forcegenerated-flexipage_giic_salesorderpage_gii__salesorder__c/flexipage-record-page-decorator/slot/flexipage-record-home-template-desktop2/div/div[1]/slot/slot/flexipage-component2[1]/force-progressive-renderer/slot/slot/records-lwc-highlights-panel/records-lwc-record-layout/records-record-layout2/force-highlights2/div[1]/div[1]/div[1]/div[2]/h1/slot/slot/lightning-formatted-text"));
			soName =  ele.getText();
			System.out.println("**********"+soName);
			Thread.sleep(8000);	
			// PAYMENT & SUBMIT ORDER SCRIPT------------------------------------

			driver.findElement(By.cssSelector("a[title=\"Pay & Submit Order\"]")).click();
			Thread.sleep(8000);		
			driver.findElement(By.xpath("//button[contains(text(), 'Payment & Submit Order')]")).click();
			Thread.sleep(8000);

			//MANUAL ALLOCATION BATCH RUN SCRIPT----------------		
			driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/header/div[3]/one-appnav/div/div/div/nav/one-app-launcher-header/button/div")).click();
			Thread.sleep(10000);
			driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div[2]/div/div[1]/h2/div/div[2]/div/div/div[2]/div[1]/input")).sendKeys("Code Maintenance", Keys.ENTER);
			Thread.sleep(10000);
			driver.findElement(By.cssSelector("a[title=\"Code Maintenance\"]")).click();
			Thread.sleep(20000);	
			driver.switchTo().frame(0);
			driver.findElement(By.id("j_id0:tabControllersys_lbl")).click();	
			Thread.sleep(10000);
			driver.findElement(By.linkText("GII")).click();
			Thread.sleep(10000);
			driver.findElement(By.cssSelector("a[title=\"Execute Allocation\"]")).click();
			Thread.sleep(10000);	   

			//Move to SO and Open fulfillment SCRIPT----------------
			maprsoresult = UtilClass.getSOData(soName); 
			soId  =  maprsoresult.get("Id");     	
			driver.get("https://royalcanin-us--d2csupport.lightning.force.com/lightning/r/gii__SalesOrder__c/"+soId+"/view");
			Thread.sleep(8000);     		
			driver.get("https://royalcanin-us--d2csupport.lightning.force.com/lightning/r/"+soId+"/related/Warehouse_Shipping_Order_Stagings1__r/view");
			Thread.sleep(8000);
			mapfulfillmentresult = UtilClass.getFulfillmentData(soId);
			fullfillmentname = mapfulfillmentresult.get("name");
			System.out.println("**********"+fullfillmentname);
			Thread.sleep(8000);
			driver.findElement(By.xpath("//a[contains(text(),'"+fullfillmentname+"')]")).click(); 


			//Process fulfillment SCRIPT----------------        
			fullfillmentId = mapfulfillmentresult.get("id");
			Thread.sleep(2000);
			//driver.get("https://royalcanin-us--d2csupport.lightning.force.com/lightning/r/gii__WarehouseShippingOrderStaging__c/"+fullfillmentId+"/view");
			//Thread.sleep(5000);
			FulfillmentIntegrationStatus = UtilClass.getFulfillmentIntegrationStatus(fullfillmentId);
			if(FulfillmentIntegrationStatus !=null && FulfillmentIntegrationStatus.contains("Calculate"))
			{
				UtilClass.updateFulfillment(fullfillmentId,"Initial");
				Thread.sleep(5000);
				driver.navigate().refresh();
				Thread.sleep(5000);
				driver.findElement(By.cssSelector("a[title=\"Calculate Tax\"]")).click();
				Thread.sleep(5000);
				driver.navigate().refresh();
				Thread.sleep(5000);
			}
			FulfillmentIntegrationStatus = UtilClass.getFulfillmentIntegrationStatus(fullfillmentId);
			if(FulfillmentIntegrationStatus !=null && FulfillmentIntegrationStatus.contains("Authorize"))
			{
				UtilClass.updateFulfillment(fullfillmentId,"Initial");
				Thread.sleep(5000);
				driver.navigate().refresh();
				Thread.sleep(5000);
				driver.findElement(By.cssSelector("a[title=\"Authorize Amount\"]")).click();
				Thread.sleep(5000);
				driver.navigate().refresh();
				Thread.sleep(5000);
			}
			else if(FulfillmentIntegrationStatus !=null && FulfillmentIntegrationStatus.contains("Sent"))
			{        	
				driver.findElement(By.cssSelector("a[title=\"Create Shipment Order\"]")).click();
				Thread.sleep(5000);
				driver.navigate().refresh();
				Thread.sleep(5000);
				driver.get("https://royalcanin-us--d2csupport.lightning.force.com/lightning/r/"+fullfillmentId+"/related/Warehouse_Shipment_Advice_Staging__r/view");	   
				Thread.sleep(2000);
			}

			mapshipmentresult = UtilClass.getShipmentData(fullfillmentId);
			shipmentname = mapshipmentresult.get("id");
			//driver.findElement(By.xpath("//a[contains(text(),'"+shipmentname+"')]")).click(); 

			driver.get("https://royalcanin-us--d2csupport.lightning.force.com/lightning/r/gii__WarehouseShipmentAdviceStaging__c/"+shipmentname+"/view");
			//Thread.sleep(2000);
			//driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div/div/div[1]/div[3]/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr/th/span")).click();
			Thread.sleep(10000);
			driver.findElement(By.cssSelector("a[title=\"Calculate Tax\"]")).click();
			Thread.sleep(10000);
			driver.navigate().refresh();  
			Thread.sleep(10000);
			driver.findElement(By.cssSelector("a[title=\"Calculate Tax\"]")).click();
			Thread.sleep(10000);  
			driver.navigate().refresh();  
			Thread.sleep(10000);
			driver.findElement(By.xpath("//a[contains(.,'Show more actions')]")).click();
			Thread.sleep(8000);
			driver.findElement(By.cssSelector("a[title=\"Create Invoice\"]")).click();
			Thread.sleep(10000);     
			driver.navigate().refresh();        
			Thread.sleep(10000);      
			driver.get("https://royalcanin-us--d2csupport.lightning.force.com/lightning/r/gii__SalesOrder__c/"+soId+"/view");
			Thread.sleep(10000);
			maprsoresult = UtilClass.getSOData(soName); 
			soStatus = maprsoresult.get("giic_SOStatus__c");
			System.out.println("**********"+soStatus);
		}
		catch(Exception e)
		{}
	}
}