package greak;

public class CountSetBitInInteger {

	public static void main(String[] args) {
		int n = 254;
		countbit(n);
		countbrain(n);
		countlookup(n);
	}

	public static void countbit(int n) {
		int c = 0;
		while (n != 0) {
			c += n & 1;
			n >>= 1;
		}
		System.out.println(c);
	}

	public static void countlookup(int n) {
		int table[] = { 0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4 };
		int c = 0;
		for (int i = 0; i < n; n >>= 4) {
			c += table[n & 0xf];
		}
		System.out.println(c);
	}

	// Brain kernighan's Algorithm
	public static void countbrain(int n) {
		/*
		 * 1.initialize count=0; 2.if(n!=zero) (a) do bitwise & with (n-1) and assin the
		 * value back to n n:=n&(n-1) (b) increment count by 1 (c) go to step 2 3. else
		 * return count
		 */
		int c = 0;
		while (n != 0) {
			n = n & (n - 1);
			c++;
		}
		System.out.println(c);
	}

	public static void convertBinary(int num) {
		int binary[] = new int[40];
		int index = 0;
		while (num > 0) {
			binary[index++] = num % 2;
			num = num / 2;
		}
		for (int i = index - 1; i >= 0; i--) {
			System.out.print(binary[i]);
		}
	}// 55

}
